from django.urls import  path

from . import views

urlpatterns = [
    path('erp/', views.index, name='erp'),   #domain/erp
    path('erp/<slug:event_name>', views.event, name='event-name'),    #domain/<dynamic>
]