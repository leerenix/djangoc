from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    events = [
        {'event': 'Не пристегнут ремень', 'id': 'belt', 'positive': False, 'danila_req': False},
        {'event': 'Сухой накурился и никуда не поедет', 'id': 'suhoy_trava', 'positive': False, 'danila_req': False},
        {'event': 'Данила набухался и никуда не поедет', 'id': 'danila_drunk', 'positive': False, 'danila_req': True},
        {'event': 'Данила довез на пол пути', 'id': 'danila_half_way', 'positive': False, 'danila_req': True},
        {'event': 'Нашел евро в кармане', 'id': 'euro_pocket', 'positive': True, 'danila_req': False},
        {'event': 'Леха позвал на работу', 'id': 'leha_work', 'positive': True, 'danila_req': False},
    ]
    return render(request, 'erp/index.html', {
        'danila': True,
        'events': events
    })


def info(request):
    return render(request, 'erp/info.html', {

    })


def event(request, event_name):
    print(event_name)
    return render(request, 'erp/event.html', {
        'name': event_name
    })